<?php get_header(); ?>

<div id="sbsroot">
    <intro-screen></intro-screen>
    <loading-screen></loading-screen>
    <router-view :key="$route.fullPath"></router-view>
</div>

<?php get_footer(); ?>