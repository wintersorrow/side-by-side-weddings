<!DOCTYPE html>
<!--[if IE 8 ]><html id="html" class="ie ie8 no-js" lang="en"> <![endif]-->
<!--[if IE 9 ]><html id="html" class="ie ie9 no-js" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html id="html" class="no-js mb" lang="en">
<!--<![endif]-->

<head>
    <script>document.getElementById("html").className = document.getElementById("html").className.replace( /(?:^|\s)no-js(?!\S)/g , 'js' )</script>
    <!--[if IE 9]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<![endif]-->
    <title>
        <?php global $page, $paged;
        wp_title('|', true, 'right');  ?>
    </title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,width=device-width" />
    <!--user-scalable=0,-->

    <?php if (is_front_page()) { ?>
		<script>
			document.documentElement.className = 'js';
		</script>
    <?php } ?>

    <?php wp_head(); ?>

    <link href="https://fonts.googleapis.com/css?family=Gelasio|Montserrat&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
</head>

<body <?php body_class(); ?>>