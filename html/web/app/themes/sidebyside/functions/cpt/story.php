<?php

use PostTypes\PostType;

/**
 * Register Story Post Type
**/
$stories = new PostType( 'stories' );

$stories->labels([
    'name'                => __( 'Story', 'sidebyside' ),
    'singular_name'       => __( 'Story', 'sidebyside' ),
    'all_items'           => __( 'All Stories', 'sidebyside' ),
    'add_new'             => __( 'Add New Story', 'sidebyside' ),
    'add_new_item'        => __( 'Add New Story', 'sidebyside' ),
    'edit_item'           => __( 'Edit Story', 'sidebyside' ),
    'new_item'            => __( 'New Story', 'sidebyside' ),
    'view_item'           => __( 'View Stories', 'sidebyside' ),
    'search_items'        => __( 'Search Stories', 'sidebyside' ),
    'not_found'           => __( 'No Stories found', 'sidebyside' ),
    'not_found_in_trash'  => __( 'No Stories found in Trash', 'sidebyside' ),
    'menu_name'           => __( 'Stories', 'sidebyside' ),
]);

$stories->options( [
    'public'                => true,
    'menu_icon'             => 'dashicons-camera',
    'has_archive'           => true,
    'hierarchical'          => true,
    'menu_position'         => null,
    'show_in_rest'          => true,
    'rewrite'               => array(
        'slug'              => 'stories'
    ),
    'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' )
] );

$stories->register();