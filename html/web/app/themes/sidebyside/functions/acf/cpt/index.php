<?php

$cpt_includes = array( 
    'story'
);

foreach( $cpt_includes as $include ) {
    $file_path = get_theme_file_path( 'functions/acf/cpt/' . $include . '.php' );
    if( file_exists( $file_path ) ) require_once $file_path;
}