<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$fc_blocks = array( 
    'image-block/block-full-width-image',
    'image-block/block-slider',
    'image-block/block-juxta'
);

$story = new FieldsBuilder( 'story_content', [
    'title' => __( 'Story Content', 'sidebyside' )
]);

$story
    ->addTab('header', [
        'label'     => __( 'Header', 'sidebyside' ),
        'placement' => 'left'
    ])
        ->addRepeater('title', [
            'label'         => __( 'Title', 'sidebyside' ),
            'instructions'  => __( 'e.g. Rosie & Jim', 'sidebyside' ),
            'layout'        => 'table',
            'button_label'  => __( 'Add Title Line', 'sidebyside' ),
        ])
            ->addText('title_line', [
                'label'     => __( 'Title Line', 'sidebyside' ),
            ])
            ->endRepeater()
        ->addText('subtitle', [
            'label'         => __( 'Subtitle', 'sidebyside' ),
            'instructions'  => __( 'Services provided e.g. Photography. Film', 'sidebyside' )
        ])
        ->addImage('feature_image', [
            'label'         => __( 'Feature Image', 'sidebyside' ),
            'return_format' => 'array',
        ])
        ->addFile('feature_video', [
            'label'         => __( 'Feature Video', 'sidebyside' ),
            'instructions'  => __( 'Recommended length 10 - 12 seconds', 'sidebyside' ),
            'return_format' => 'array'
        ])
    ->addTab('about', [
        'label'     => __( 'About', 'sidebyside' ),
        'placement' => 'left'
    ])
        ->addText('services', [
            'label'         => __( 'Services', 'sidebyside' ),
        ])
        ->addText('event_date', [
            'label'         => __( 'Event Date', 'sidebyside' ),
        ])
        ->addText('event_location', [
            'label'         => __( 'Event Location', 'sidebyside' ),
        ])
        ->addTextArea('description', [
            'label'         => __( 'Description', 'sidebyside' ),
        ])
    ->setLocation('post_type', '==', 'stories')
    ->setGroupConfig('hide_on_screen', [
        'permalink',
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'revisions',
        'slug',
        'author',
        'format',
        'featured_image',
        'categories',
        'tags',
        'send-trackbacks'
    ]);

$flexibleContent = $story
    ->addTab('photos', [
            'label' => __( 'Photos', 'sidebyside' ),
            'placement' => 'left'
        ])
        ->addFlexibleContent('photos', [
            'button_label' => __( 'Add Block', 'sidebyside' ),
        ]);

foreach( $fc_blocks as $fc_block ) {
    $current_fc_block = require get_theme_file_path( 'functions/acf/flexible-content/' . $fc_block . '.php' );

    if( $current_fc_block ) {
        $flexibleContent->addLayout( $current_fc_block ) ;
    }
}

add_action('acf/init', function() use ( $story ) {
    acf_add_local_field_group( $story->build() );
});