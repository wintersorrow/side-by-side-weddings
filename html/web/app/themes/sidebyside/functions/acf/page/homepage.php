<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$homepage = new FieldsBuilder( 'homepage', [
    'title' => __( 'Homepage Content', 'sidebyside' )
]);

$homepage
    ->addTab('featured_stories', [
        'label'     => __( 'Featured Stories', 'sidebyside' ),
        'placement' => 'left'
    ])
        ->addRepeater('featured_stories', [
            'label'         => __( 'Featured Stories', 'sidebyside' ),
            'layout'        => 'block',
            'button_label'  => __( 'Add Featured Story', 'sidebyside' ),
        ])
            ->addPostObject('story', [
                'label'         => __( 'Story', 'hisense' ),
                'post_type'     => 'stories',
                'return_format' => 'object'
            ])
            ->addRepeater('title', [
                'label'         => __( 'Title', 'hisense' ),
                'layout'        => 'table',
                'button_label'  => __( 'Add Title Line', 'hisense' ),
            ])
                ->addText('title_line', [
                    'label'         => __( 'Title Line', 'hisense' ),
                ])
                ->endRepeater()
            ->addText('subtitle', [
                'label'         => __( 'Subtitle', 'sidebyside' ),
            ])
        ->endRepeater()
    ->setLocation('post_type', '==', 'page')
    ->and('page_type', '==', 'front_page')
    ->setGroupConfig('hide_on_screen', [
        'permalink',
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'revisions',
        'slug',
        'author',
        'format',
        'featured_image',
        'categories',
        'tags',
        'send-trackbacks'
    ]);

add_action('acf/init', function() use ( $homepage ) {
    acf_add_local_field_group( $homepage->build() );
});