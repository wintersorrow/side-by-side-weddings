<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$aboutPage = new FieldsBuilder( 'about_page', [
    'title' => __( 'About Page', 'sidebyside' )
]);

$aboutPage
    ->addTab('page_header', [
        'label'     => __( 'Header', 'sidebyside' ),
        'placement' => 'left'
    ])
        ->addText('title', [
            'label'         => __( 'Title', 'sidebyside' ),
        ])
        ->addText('subtitle', [
            'label'         => __( 'Subtitle', 'sidebyside' ),
        ])
    ->addTab('content', [
        'label'     => __( 'Content', 'sidebyside' ),
        'placement' => 'left'
    ])
        ->addImage('featured_image', [
            'label'         => __( 'Featured Image', 'sidebyside' ),
            'return_format' => 'array',
        ])
        ->addRepeater('content', [
            'label'         => __( 'Content', 'sidebyside' ),
            'layout'        => 'block',
            'button_label'  => __( 'Add Content Block', 'sidebyside' ),
        ])
            ->addSelect('content_type', [
                'label'             => __( 'Content Type', 'sidebyside' ),
                'choices'           => [
                    'standard'      => __( 'Standard', 'sidebyside' ),
                    'services'      => __( 'Services', 'sidebyside' ),
                    'pricing'       => __( 'Pricing', 'sidebyside' )
                ],
                'default_value'     => 'standard',
            ])
            ->addText('title', [
                'label'         => __( 'Title', 'sidebyside' )
            ])
            ->addWysiwyg('content', [
                'label'         => __( 'Content', 'sidebyside' ),
                'tabs'          => 'all',
                'toolbar'       => 'full'
            ])
            ->addRepeater('services', [
                'label'         => __( 'Service', 'sidebyside' ),
                'layout'        => 'block',
                'button_label'  => __( 'Add Service', 'sidebyside' )
            ])->conditional('content_type', '==', 'services')
                ->addText('service', [
                    'label'     => __( 'Service', 'sidebyside' )
                ])
                ->endRepeater()
            ->addRepeater('prices', [
                'label'         => __( 'Prices', 'sidebyside' ),
                'layout'        => 'block',
                'button_label'  => __( 'Add Price', 'sidebyside' )
            ])->conditional('content_type', '==', 'pricing')
                ->addText('title', [
                    'label'     => __( 'Title', 'sidebyside' )
                ])
                ->addTextarea('description', [
                    'label'     => __( 'Description', 'sidebyside' )
                ])
                ->addText('price', [
                    'label'     => __( 'Price', 'sidebyside' )
                ])
                ->endRepeater()
            ->addRepeater('extra_prices', [
                'label'         => __( 'Extra Prices', 'sidebyside' ),
                'layout'        => 'block',
                'button_label'  => __( 'Add Price', 'sidebyside' )
            ])->conditional('content_type', '==', 'pricing')
                ->addText('title', [
                    'label'     => __( 'Title', 'sidebyside' )
                ])
                ->addTextarea('description', [
                    'label'     => __( 'Description', 'sidebyside' )
                ])
                ->addText('price', [
                    'label'     => __( 'Price', 'sidebyside' )
                ])
                ->endRepeater()
            ->addTrueFalse('in_page_link', [
                'label'         => __( 'In Page Link?', 'sidebyside' ),
                'wrapper'       => [
                    'width'     => '30',
                ],
            ])
            ->addText('link_title', [
                'label'         => __( 'Link Title', 'sidebyside' ),
                'instructions'  => __( 'Specific title for in-page menu.', 'sidebyside' ),
                'wrapper'       => [
                    'width'     => '70',
                ]
            ])
            ->conditional('in_page_link', '==', 1)
        ->endRepeater()
    ->addTab('contact_page', [
        'label'     => __( 'Contact Page', 'sidebyside' ),
        'placement' => 'left'
    ])
        ->addTrueFalse('show_link_to_contact_page', [
            'label'         => __( 'Show link to contact page?', 'sidebyside' ),
            'wrapper'       => [
                'width'     => '30',
            ],
        ])
        ->addText('contact_link_text', [
            'label'         => __( 'Link Text', 'sidebyside' ),
            'wrapper'       => [
                'width'     => '70',
            ]
        ])
        ->conditional('show_link_to_contact_page', '==', 1)
    ->setLocation('post_type', '==', 'page')
    ->and('page', '==', '99')
    ->setGroupConfig('hide_on_screen', [
        'permalink',
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'revisions',
        'slug',
        'author',
        'format',
        'featured_image',
        'categories',
        'tags',
        'send-trackbacks'
    ]);

add_action('acf/init', function() use ( $aboutPage ) {
    acf_add_local_field_group( $aboutPage->build() );
});