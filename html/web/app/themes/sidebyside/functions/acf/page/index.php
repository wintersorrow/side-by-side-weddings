<?php

$page_acf_includes = array( 
    'about',
    'contact',
    'homepage',
    'portfolio'
);

foreach( $page_acf_includes as $include ) {
    $file_path = get_theme_file_path( 'functions/acf/page/' . $include . '.php' );
    if( file_exists( $file_path ) ) require_once $file_path;
}