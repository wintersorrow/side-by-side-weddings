<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$contactPage = new FieldsBuilder( 'contact_page', [
    'title' => __( 'Contact Page', 'sidebyside' )
]);

$contactPage
    ->addTab('page_header', [
        'label'     => __( 'Header', 'sidebyside' ),
        'placement' => 'left'
    ])
        ->addText('title', [
            'label'         => __( 'Title', 'sidebyside' ),
        ])
    ->addTab('content', [
        'label'     => __( 'Content', 'sidebyside' ),
        'placement' => 'left'
    ])
        ->addWysiwyg('content', [
            'label'         => __( 'Content', 'sidebyside' ),
            'tabs'          => 'all',
            'toolbar'       => 'full'
        ])
        ->addRepeater('links', [
            'label'         => __( 'Links', 'sidebyside' ),
            'layout'        => 'block',
            'button_label'  => __( 'Add Link', 'sidebyside' ),
        ])
            ->addText('link', [
                'label'         => __( 'Link', 'sidebyside' ),
                'wrapper'       => [
                    'width'     => '50',
                ],
            ])
            ->addText('link_text', [
                'label'         => __( 'Link Text', 'sidebyside' ),
                'wrapper'       => [
                    'width'     => '50',
                ],
            ])
        ->endRepeater()
    ->setLocation('post_type', '==', 'page')
    ->and('page', '==', '71')
    ->setGroupConfig('hide_on_screen', [
        'permalink',
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'revisions',
        'slug',
        'author',
        'format',
        'featured_image',
        'categories',
        'tags',
        'send-trackbacks'
    ]);

add_action('acf/init', function() use ( $contactPage ) {
    acf_add_local_field_group( $contactPage->build() );
});