<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$portfolio = new FieldsBuilder( 'portfolio', [
    'title' => __( 'Portfolio', 'sidebyside' )
]);

$portfolio
    ->addGallery('gallery', [
        'label'         => __( 'Gallery', 'hisense' ),
        'return_format' => 'array'
    ])
    ->setLocation('post_type', '==', 'page')
    ->and('page', '==', '53')
    ->setGroupConfig('hide_on_screen', [
        'permalink',
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'revisions',
        'slug',
        'author',
        'format',
        'featured_image',
        'categories',
        'tags',
        'send-trackbacks'
    ]);

add_action('acf/init', function() use ( $portfolio ) {
    acf_add_local_field_group( $portfolio->build() );
});