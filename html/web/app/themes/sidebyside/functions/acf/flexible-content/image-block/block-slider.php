<?php 

use StoutLogic\AcfBuilder\FieldsBuilder;

$fc_block = new FieldsBuilder('slider', [
    'label' => __( 'Slider', 'sidebyside' )
]);

$fc_block
    ->addGallery('gallery', [
        'label'         => __( 'Gallery', 'sidebyside' ),
        'return_format' => 'array'
    ]);

return $fc_block;