
<?php 

use StoutLogic\AcfBuilder\FieldsBuilder;

$fc_block = new FieldsBuilder('juxta_blocks', [
    'label' => __( 'Juxta Blocks', 'sidebyside' )
]);

$fc_block
    ->addImage('image_left', [
        'label'         => __( 'Left Image', 'sidebyside' ),
        'return_format' => 'array',
        'wrapper'       => [
            'width'     => '50',
        ]
    ])
    ->addImage('image_right', [
        'label'         => __( 'Right Image', 'sidebyside' ),
        'return_format' => 'array',
        'wrapper'       => [
            'width'     => '50',
        ]
    ])
    ->addGroup('layout', [
        'label' => __( 'Layout', 'sidebyside' ),
        'layout' => 'block'
    ])
        ->addSelect('alignment', [
            'label'             => __( 'Alignment', 'sidebyside' ),
            'instructions'      => __( 'How to align the first image vertically.', 'sidebyside' ),
            'choices'           => [
                'offset-bottom' => __( 'Bottom (Offset)', 'sidebyside' ),
                'middle'        => __( 'Middle', 'sidebyside' ),
                'top'           => __( 'Top (Both)', 'sidebyside' ),
                'offset-top'    => __( 'Top (Offset)', 'sidebyside' )
            ],
            'wrapper'           => [
                'width'         => '50',
            ],
        ])
        ->addSelect('image_widths', [
            'label'         => __( 'Image Widths', 'sidebyside' ),
            'choices'       => [
                '30-70'     => __( '30/70', 'sidebyside' ),
                '40-60'     => __( '40/60', 'sidebyside' ),
                '50-50'     => __( '50/50', 'sidebyside' ),
                '60-40'     => __( '60/40', 'sidebyside' ),
                '70-30'     => __( '70/30', 'sidebyside' ),
            ],
            'wrapper'       => [
                'width'     => '50',
            ],
        ])
        ->addSelect('spacing', [
            'label'         => __( 'Spacing (Between Images)', 'sidebyside' ),
            'choices'       => [
                'none'      => __( 'No Spacing', 'sidebyside' ),
                'small'     => __( 'Small Spacing', 'sidebyside' ),
                'medium'    => __( 'Medium Spacing', 'sidebyside' ),
                'large'     => __( 'Large Spacing', 'sidebyside' )
            ],
            'wrapper'       => [
                'width'     => '50',
            ],
        ])
        ->addSelect('side_spacing', [
            'label'         => __( 'Spacing (On Sides)', 'sidebyside' ),
            'choices'       => [
                'none'      => __( 'No Spacing', 'sidebyside' ),
                'small'     => __( 'Small Spacing', 'sidebyside' ),
                'medium'    => __( 'Medium Spacing', 'sidebyside' ),
                'large'     => __( 'Large Spacing', 'sidebyside' )
            ],
            'wrapper'       => [
                'width'     => '50',
            ],
        ])
        ->endGroup();

return $fc_block;