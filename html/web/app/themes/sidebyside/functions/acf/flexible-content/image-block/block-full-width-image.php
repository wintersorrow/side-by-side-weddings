<?php 

use StoutLogic\AcfBuilder\FieldsBuilder;

$fc_block = new FieldsBuilder('full_width_image', [
    'label' => __( 'Full Width Image', 'sidebyside' )
]);

$fc_block
    ->addImage('image', [
        'label'         => __( 'Image', 'sidebyside' ),
        'return_format' => 'array'
    ])
    ->addGroup('layout', [
        'label' => __( 'Layout', 'sidebyside' ),
        'layout' => 'block'
    ])
        ->addSelect('side_spacing', [
            'label'         => __( 'Spacing (On Sides)', 'sidebyside' ),
            'choices'       => [
                'none'      => __( 'No Spacing', 'sidebyside' ),
                'small'     => __( 'Small Spacing', 'sidebyside' ),
                'medium'    => __( 'Medium Spacing', 'sidebyside' ),
                'large'     => __( 'Large Spacing', 'sidebyside' )
            ],
            'wrapper'       => [
                'width'     => '50',
            ],
        ])
        ->endGroup();

return $fc_block;