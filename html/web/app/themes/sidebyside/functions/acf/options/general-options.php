<?php

use StoutLogic\AcfBuilder\FieldsBuilder;

$generalOptions = new FieldsBuilder( 'general_options', [
    'title' => __( 'General Options', 'sidebyside' )
]);

$generalOptions
    ->addTab('general', [
        'label'     => __( 'General', 'sidebyside' ),
        'placement' => 'left'
    ])
        ->addRepeater('contact_details', [
            'label'         => __( 'Contact Details', 'sidebyside' ),
            'layout'        => 'table',
            'button_label'  => __( 'Add Contact Detail', 'sidebyside' ),
        ])
            ->addSelect('type', [
                'label'             => __( 'Type', 'sidebyside' ),
                'choices'           => [
                    'email'         => __( 'Email', 'sidebyside' ),
                    'phone'         => __( 'Phone', 'sidebyside' )
                ],
            ])
            ->addText('detail', [
                'label'         => __( 'Detail', 'sidebyside' ),
            ])
            ->endRepeater()
        ->addRepeater('social_accounts', [
            'label'         => __( 'Social Accounts', 'sidebyside' ),
            'layout'        => 'table',
            'button_label'  => __( 'Add Social Account', 'sidebyside' ),
        ])
            ->addSelect('type', [
                'label'             => __( 'Type', 'sidebyside' ),
                'choices'           => [
                    'facebook'      => __( 'Facebook', 'sidebyside' ),
                    'instagram'     => __( 'Instagram', 'sidebyside' ),
                    'twitter'       => __( 'Twitter', 'sidebyside' ),
                ],
            ])
            ->addText('detail', [
                'label'         => __( 'Detail', 'sidebyside' ),
            ])
            ->endRepeater()
    ->setLocation('options_page', '==', 'acf-options-site-options')
    ->setGroupConfig('hide_on_screen', [
        'permalink',
        'the_content',
        'excerpt',
        'discussion',
        'comments',
        'revisions',
        'slug',
        'author',
        'format',
        'featured_image',
        'categories',
        'tags',
        'send-trackbacks'
    ]);

add_action('acf/init', function() use ( $generalOptions ) {
    acf_add_local_field_group( $generalOptions->build() );
});