<?php

$option_includes = array( 
    'option-pages',
    'general-options'
);

foreach( $option_includes as $include ) {
    $file_path = get_theme_file_path( 'functions/acf/options/' . $include . '.php' );
    if( file_exists( $file_path ) ) require_once $file_path;
}