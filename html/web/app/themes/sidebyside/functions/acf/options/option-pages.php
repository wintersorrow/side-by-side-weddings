<?php

// ACF
if ( function_exists('acf_add_options_page') ) {
    acf_add_options_page(
        array(
            'page_title'    => 'Site Options',
            'icon_url'      => 'dashicons-list-view',
            'post_id'       => 'general-settings',
            'show_in_rest'  => true,
        )
    );
}