<?php

// Include Option ACF Settings
if( file_exists( get_theme_file_path( 'functions/acf/options/index.php' ) ) )
    require_once get_theme_file_path( 'functions/acf/options/index.php' );

// Include CPT ACF Settings
if( file_exists( get_theme_file_path( 'functions/acf/cpt/index.php' ) ) )
    require_once get_theme_file_path( 'functions/acf/cpt/index.php' );

// Include Page ACF Settings
if( file_exists( get_theme_file_path( 'functions/acf/page/index.php' ) ) )
    require_once get_theme_file_path( 'functions/acf/page/index.php' );
