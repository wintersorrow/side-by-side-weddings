<?php 

// Turn off errors
ini_set(' display_errors', 0 );

// Hide admin bar on the fly
add_filter( 'show_admin_bar', 'hide_admin_bar' );

function hide_admin_bar( $content ) {
    return ( isset( $_GET['adminbar'] ) && $_GET['adminbar'] != 'true ') ? $content : false;
}


// Remove customiser link in adminbar introduced in 4.3
add_action( 'admin_bar_menu', 'remove_customize', 999 );

function remove_customize( $wp_admin_bar ) {
    $wp_admin_bar->remove_node('customize');
    $wp_admin_bar->remove_node('customize-background');
    $wp_admin_bar->remove_node('customize-header');
    $wp_admin_bar->remove_node('customize-themes');
    $wp_admin_bar->remove_node('customize-widgets');
}


add_action( 'admin_menu', 'remove_menus' );

function remove_menus() {
    remove_menu_page('edit.php'); //Posts
    remove_menu_page('edit-comments.php'); //Comments
}


// Disable Gutenburg
add_filter( 'use_block_editor_for_post', '__return_false', 10 );