<?php

add_action( 'login_enqueue_scripts', 'sbs_login_logo' );

function sbs_login_logo() { ?>
    <style type="text/css">
        #login h1 a,
        .login h1 a {
        height:100px;
        width:300px;
        background-size: 300px 100px;
        background-repeat: no-repeat;
        padding-bottom: 10px;
        }
    </style>
<?php }