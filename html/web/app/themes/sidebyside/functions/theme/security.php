<?php

// Disable XMLRPC
add_filter( 'xmlrpc_enabled', '__return_false' );

// Prevent admin username sniffing
if( isset( $_GET['author'] ) ) { header( 'Location: '.get_site_url() ); die(); }

// Remove Wordpress Version in source
add_filter( 'the_generator', 'wp_version_remove_version' );

function wp_version_remove_version() {
	return '';
}

// Remove WP version from core assets
add_filter( 'style_loader_src', 'remove_css_js_version', 9999 );
add_filter( 'script_loader_src', 'remove_css_js_version', 9999 );

function remove_css_js_version($src) {
    if( strpos( $src, '?ver=' ) && !strpos( $src, 'themes/sidebyside' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

// Remove unnecessary elements from wp_head
remove_action( 'wp_head','rsd_link' );
remove_action( 'wp_head','wp_generator' );
remove_action( 'wp_head','feed_links', 2 );
remove_action( 'wp_head','index_rel_link' );
remove_action( 'wp_head','wlwmanifest_link' );
remove_action( 'wp_head','feed_links_extra', 3 );
remove_action( 'wp_head','start_post_rel_link', 10, 0 );
remove_action( 'wp_head','parent_post_rel_link', 10, 0 );
remove_action( 'wp_head','adjacent_posts_rel_link', 10, 0 );

// Remove emoji	introduced in 4.2
add_action( 'init', 'disable_wp_emojicons' );
function disable_wp_emojicons() {
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles ');
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email ');
    remove_filter( 'the_content_feed', 'wp_staticize_emoji ');
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji ');
}