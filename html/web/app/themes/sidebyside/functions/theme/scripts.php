<?php

add_filter( 'clean_url', 'async_scripts', 11, 1 );

function async_scripts( $url ) {
    if ( strpos( $url, '#asyncload' ) === false ) {
        return $url;
    } else if ( is_admin() ) {
        return str_replace( '#asyncload', '', $url );
    } else {
        return str_replace( '#asyncload', '', $url ) . "' async='async";
    }
}


// Enqueue styles and scripts
if ( !is_admin() ) {
    add_action( 'wp_enqueue_scripts', 'sbs_enqueue_scripts' );
}

function sbs_enqueue_scripts() {
    wp_enqueue_style('css-styles', get_template_directory_uri() . '/assets/dist/styles/style.css', [], filemtime( get_template_directory() . '/assets/dist/styles/style.css') );
    wp_enqueue_script('lazysizes', get_template_directory_uri() . '/assets/dist/scripts/lazysizes.min.js#asyncload', [], '4.1.6', true);

    wp_enqueue_script('js-scripts-manifest', get_template_directory_uri() . '/assets/dist/scripts/manifest.js', [] , filemtime( get_template_directory() . '/assets/dist/scripts/manifest.js'), true);
    wp_enqueue_script('js-scripts-vendor', get_template_directory_uri() . '/assets/dist/scripts/vendor.js', array('js-scripts-manifest'), filemtime( get_template_directory() . '/assets/dist/scripts/vendor.js'), true);
    wp_enqueue_script('js-scripts', get_template_directory_uri() . '/assets/dist/scripts/scripts.js#asyncload', array('js-scripts-vendor'), filemtime( get_template_directory() . '/assets/dist/scripts/scripts.js'), true);
    // wp_enqueue_script('js-scripts', get_template_directory_uri() . '/assets/dist/scripts/scripts.js#asyncload', array('jquery'), filemtime( get_template_directory() . '/assets/dist/scripts/scripts.js'), true);
}


add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.4.1.min.js", array(), '3.4.1' );
}


add_action( 'wp_head', 'sbs_site_favicons' );

function sbs_site_favicons() {
    $icons = '';
    $icons .= '<link rel="apple-touch-icon" sizes="180x180" href="' . get_template_directory_uri() . '/assets/dist/images/favicon/apple-touch-icon.png">';
    $icons .= '<link rel="icon" type="image/png" sizes="32x32" href="' . get_template_directory_uri() . '/assets/dist/images/favicon/favicon-32x32.png">';
    $icons .= '<link rel="icon" type="image/png" sizes="16x16" href="' . get_template_directory_uri() . '/assets/dist/images/favicon/favicon-16x16.png">';
    $icons .= '<link rel="manifest" href="' . get_template_directory_uri() . '/assets/dist/images/favicon/site.webmanifest">';
    $icons .= '<link rel="mask-icon" href="' . get_template_directory_uri() . '/assets/dist/images/favicon/safari-pinned-tab.svg" color="#0c0c0c">';
    $icons .= '<link rel="shortcut icon" href="' . get_template_directory_uri() . '/assets/dist/images/favicon/favicon.ico">';
    $icons .= '<meta name="msapplication-TileColor" content="#0c0c0c">';
    $icons .= '<meta name="msapplication-config" content="' . get_template_directory_uri() . '/assets/dist/images/favicon/browserconfig.xml">';
    $icons .= '<meta name="theme-color" content="#0c0c0c">';

    echo $icons;
}