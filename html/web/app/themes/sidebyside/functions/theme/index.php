<?php

$theme_includes = array( 
    'base',
    'login',
    'scripts',
    'security'
);

foreach( $theme_includes as $include ) {
    $file_path = get_theme_file_path( 'functions/theme/' . $include . '.php' );
    if( file_exists( $file_path ) ) require_once $file_path;
}