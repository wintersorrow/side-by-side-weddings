<?php 

// Theme Specific Code. This code forms the basis of the theme, and is essential for the theme to function.
require_once get_theme_file_path( 'functions/theme/index.php' );

// Custom Post Types. All registration of new cpt's takes place within this directory.
require_once get_theme_file_path( 'functions/cpt/index.php' );

// ACF Specific Code. Create field groups and options pages
require_once get_theme_file_path( 'functions/acf/index.php' );

// API Config. All rest routes and endpoints necessary accessible by the frontend are registered here.
require_once get_theme_file_path( 'functions/api/index.php' );