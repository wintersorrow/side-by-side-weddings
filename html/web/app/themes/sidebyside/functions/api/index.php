<?php

$api_includes = array( 
    'homepage',
    'story',
    'general',
    'yoast'
);

foreach( $api_includes as $include ) {
    $file_path = get_theme_file_path( 'functions/api/' . $include . '.php' );
    if( file_exists( $file_path ) ) require_once $file_path;
}