<?php

if ( !function_exists( 'sbs_get_homepage_route' ) ) {

    add_action( 'rest_api_init', 'sbs_get_homepage_route' );

    function sbs_get_homepage_route() {
        register_rest_route(
            'sbs/v1',
            'homepage',
            array(
                'methods' => 'GET',
                'callback' => 'sbs_get_homepage'
            )
        );
    }

    function sbs_get_homepage( WP_REST_Request $request ) {
        $output = [];
        $code = 200;

        $homepage_id = get_option('page_on_front');

        $homepage = get_post( $homepage_id );

        if( $homepage ) {

            $output['page'] = $homepage;

            $featured_stories = get_field( 'featured_stories', $homepage_id );

            foreach( $featured_stories as $featured_story ) {
                $output['featured_stories'][] = array(
                    'title'         => $featured_story['title'],
                    'subtitle'      => $featured_story['subtitle'],
                    'feature_image' => get_field( 'feature_image', $featured_story['story']->ID ),
                    'feature_video' => get_field( 'feature_video', $featured_story['story']->ID ),
                    'link'          => '/stories/' . $featured_story['story']->post_name
                );
            }

        } else {
            $code = 204;
        }

        return new WP_REST_Response($output, $code);
    }
}
