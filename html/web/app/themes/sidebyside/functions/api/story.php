<?php

if ( !function_exists( 'sbs_get_next_story' ) ) {

    add_action( 'rest_api_init', 'sbs_get_next_story_route' );

    function sbs_get_next_story_route() {
        register_rest_route(
            'sbs/v1',
            'next_story',
            array(
                'methods' => 'GET',
                'callback' => 'sbs_get_next_story'
            )
        );
    }

    function sbs_get_next_story( WP_REST_Request $request ) {
        global $post;
        $params = $request->get_params();

        $post = get_post( $params['story_id'] );

        $output = [];
        $code = 200;

        if( $params['story_id'] !== null ) {
            $previous = get_previous_post();
            $next = get_next_post();

            if( $next !== '' ) {
                $link = get_the_permalink( $next->ID );
                $link = parse_url( $link );

                $title = 'View';

                foreach( get_field( 'title', $next->ID ) as $title_part ) {
                    $title .= " " . $title_part['title_line'];
                }

                if( $title !== 'View' ) {
                    $title = $title . "'s story";
                }

                $output = array(
                    'title' => $title,
                    'link'  => $link['path']
                );
            } else {
                if( $previous !== '' ) {
                    $link = get_the_permalink( $previous->ID );
                    $link = parse_url( $link );

                    $title = 'View';

                    foreach( get_field( 'title', $previous->ID ) as $title_part ) {
                        $title .= " " . $title_part['title_line'];
                    }

                    if( $title !== 'View' ) {
                        $title = $title . "'s story";
                    }
                    
                    $output = array(
                        'title' => $title,
                        'link'  => $link['path']
                    );
                }
            }
        } else {
            $code = 204;
        }

        return new WP_REST_Response($output, $code);
    }
}