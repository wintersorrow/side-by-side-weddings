<?php 

/**
 * Register the custom wp-json endpoints so they will be cached.
 */
function wprc_add_acf_posts_endpoint( $allowed_endpoints ) {
    if ( ! isset( $allowed_endpoints[ 'acf/v3/options' ] ) || ! in_array( 'general-settings', $allowed_endpoints[ 'acf/v3/options' ] ) ) {
        $allowed_endpoints[ 'acf/v3/options' ][] = 'general-settings';
    }
    if ( ! isset( $allowed_endpoints[ 'sbs/v1' ] ) || ! in_array( 'slider', $allowed_endpoints[ 'sbs/v1' ] ) ) {
        $allowed_endpoints[ 'sbs/v1' ][] = 'slider';
    }
    if ( ! isset( $allowed_endpoints[ 'sbs/v1' ] ) || ! in_array( 'homepage', $allowed_endpoints[ 'sbs/v1' ] ) ) {
        $allowed_endpoints[ 'sbs/v1' ][] = 'homepage';
    }
    if ( ! isset( $allowed_endpoints[ 'sbs/v1' ] ) || ! in_array( 'yoast', $allowed_endpoints[ 'sbs/v1' ] ) ) {
        $allowed_endpoints[ 'sbs/v1' ][] = 'yoast';
    }

    return $allowed_endpoints;
}
add_filter( 'wp_rest_cache/allowed_endpoints', 'wprc_add_acf_posts_endpoint', 10, 1);

add_filter( 'rest_prepare_post', function( $response, $post, $request ) {
    if( $request->get_param('per_page') === 1 ) {
        global $post;
        $next = get_adjacent_post( false, '', false );
        $previous = get_adjacent_post( false, '', true );

        $response->data['next'] = ( is_a( $next, 'WP_Post') ) ? array( "id" => $next->ID, "slug" => $next->post_name ) : null;
        $response->data['previous'] = ( is_a( $previous, 'WP_Post') ) ? array( "id" => $previous->ID, "slug" => $previous->post_name ) : null;
    }
  
    return $response;
}, 10, 3 );