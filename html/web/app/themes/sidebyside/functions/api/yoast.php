<?php

if( !function_exists('get_yoast_data_route') ) {
    function get_yoast_data_route() {
        register_rest_route(
            'sbs/v1',
            'yoast/(?P<post>[0-9]+)',
            [
                'methods' => 'GET',
                'callback' => 'getYoastData'
            ]);
    }
    add_action('rest_api_init','get_yoast_data_route');

    function getYoastData( WP_REST_Request $request ) {
        $output = [];
        $output['title'] = yoastField( $request->get_param('post') );
        $output['description'] = yoastField( $request->get_param('post'), '_yoast_wpseo_metadesc' );
        $code = 200;
        return new WP_REST_Response($output, $code);
    }

    function yoastField($post_id, $field = '_yoast_wpseo_title') {
        $yoast_title = get_post_meta($post_id, $field, true);
        $title = strstr($yoast_title, '%%', true);
        if( $title === false ){
            $title = $yoast_title;
        }
        return $title;
    }
}