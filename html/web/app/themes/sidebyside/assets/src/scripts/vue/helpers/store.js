import { createStore } from 'vuex';
import VuexPersist from 'vuex-persist';

new VuexPersist({
    key: 'vuex',
    storage: window.localStorage,
    reducer: state => ({
        show_intro: state.show_intro,
        is_loading: state.is_loading,
        page: state.page,
        story_header: state.story_header,
        settings: state.settings,
        referrer: state.referrer
    })
});

const store = createStore({
    state: {
        show_intro: true,
        is_loading: true,
        page: null,
        theme: 'dark',
        settings: null,
        referrer: false,
        story_header: {
            content: null,
            slideCount: 0,
            slideIndex: 0
        }
    },
    mutations: {
        UPDATE_INTRO_STATUS(state, status) {
            state.show_intro = status;
        },
        UPDATE_LOADING_STATUS(state, status) {
            state.is_loading = status;
        },
        UPDATE_PAGE(state, page) {
            state.page = page;
        },
        UPDATE_SETTINGS(state, settings) {
            state.settings = settings;
        },
        UPDATE_STORY_HEADER(state, story_header) {
            if( story_header.content !== undefined ){
                state.story_header.content = story_header.content;
            }
            if( story_header.slideCount !== undefined ){
                state.story_header.slideCount = story_header.slideCount;
            }
            if( story_header.slideIndex !== undefined ){
                state.story_header.slideIndex = story_header.slideIndex;
            }
        },
        UPDATE_THEME(state, theme) {
            state.theme = theme;
        },
        UPDATE_REFERRER(state, referrer) {
            state.referrer = referrer;
        },
    },
    actions: {
        updatePage(context, page) {
            context.commit('UPDATE_PAGE', page );

            var url = '';
            
            if( page.link !== undefined ) {
                url = page.link;
                url = url.replace( window.location.origin, "" );
            }

            let title = htmlDecode(page.title.rendered);
            let description = '';

            axios.get('/wp-json/sbs/v1/yoast/'+page.id).then(response => {
                if(response.data.title !== undefined && response.data.title.length > 0){
                    title = htmlDecode(response.data.title);
                }
                if( ( response.data.description !== undefined && response.data.description.length > 0 ) ){
                    description = response.data.description;
                    $('meta[name=description]').remove();
                    $('head').append( '<meta name="description" content="'+description+'">' );
                }

                title = title.replace(' | Side By Side Weddings', '');

                document.title = title + ' | Side By Side Weddings';

            })
            .catch( (error) => {});

        },
        isLoading( context, status ) {
            context.commit('UPDATE_LOADING_STATUS', status);
        },
        updateIntro( context, status ) {
            context.commit('UPDATE_INTRO_STATUS', status);
        },
        updateStoryHeader( context, story_header ) {
            context.commit('UPDATE_STORY_HEADER', {
                content: story_header.content,
                slideCount: story_header.slideCount,
                slideIndex: story_header.slideIndex
            });
        },
        updateSettings( context, settings ) {
            context.commit('UPDATE_SETTINGS', settings);
        },
        updateTheme( context, theme ) {
            context.commit('UPDATE_THEME', theme);
        },
        updateReferrer( context, referrer ) {
            context.commit('UPDATE_REFERRER', referrer);
        }
    },
});

export default store;

function htmlDecode(value) {
    return $("<textarea/>").html(value).text();
}