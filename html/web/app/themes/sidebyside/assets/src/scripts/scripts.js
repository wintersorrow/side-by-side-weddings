import $ from 'jquery';

import { TweenMax, TimelineMax } from "gsap";
import { ScrollToPlugin } from "gsap/ScrollToPlugin";
import CSSRulePlugin from 'gsap/CSSRulePlugin';
import TweenLite from 'gsap/TweenLite';

import axios from "axios";
import Swiper from "swiper";

import { createApp } from 'vue';
import Vuex from 'vuex';
import moment from 'moment';

import AOS from "AOS";

window.$ = $;
window.AOS = AOS;
window.axios = axios;
window.createApp = createApp;
window.Vuex = Vuex;
window.TweenMax = TweenMax;
window.TweenLite = TweenLite;
window.TimelineMax = TimelineMax;
window.ScrollToPlugin = ScrollToPlugin;
window.CSSRulePlugin = CSSRulePlugin;

window.Swiper = Swiper;
window.moment = moment;

import Cursor from "./components/cursor.js";

window.cursor = new Cursor();

require("./vue/vue.js");