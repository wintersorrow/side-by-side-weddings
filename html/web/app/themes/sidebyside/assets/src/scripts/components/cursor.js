import { TimelineMax, Power0 } from "gsap";

export default class Cursor {

    constructor() {

        this.cursor = jQuery(".cursor");
        this.follower = jQuery(".cursor-follower");

        this.posX = 0;
        this.posY = 0;
        
        this.mouseX = 0;
        this.mouseY = 0;

        this.init();
        this.eventListeners();
    }

    init() {
        var self = this;

        TweenMax.to({}, 0.016, {
            repeat: -1,
            onRepeat: function () {
                self.posX += (self.mouseX - self.posX) / 9;
                self.posY += (self.mouseY - self.posY) / 9;
        
                TweenMax.set(self.follower, {
                    css: {
                        left: self.posX - 12,
                        top: self.posY - 12
                    }
                });
        
                TweenMax.set(self.cursor, {
                    css: {
                        left: self.mouseX,
                        top: self.mouseY
                    }
                });
            }
        });
    }

    eventListeners(){
        var self = this;

        $(document).on("mousemove", function (e) {
            self.mouseX = e.clientX;
            self.mouseY = e.clientY;
        
            self.cursor.addClass("enabled");
            self.follower.addClass("enabled");
        });
        
        $("body").on( "mouseenter", "a", function () {
            self.cursor.addClass("active");
            self.follower.addClass("active");
        });
        
        $("body").on( "mouseleave", "a", function () {
            self.cursor.removeClass("active");
            self.follower.removeClass("active");
        });
        
        $("body").on( "mouseenter", ".portfolio .swiper-slide img, .photos-block__slider .swiper-slide img", function () {
            self.cursor.addClass("active drag");
            self.follower.addClass("active drag");
        });

        $("body").on( "mouseleave", ".portfolio .swiper-slide img, .photos-block__slider .swiper-slide img", function () {
            self.cursor.removeClass("active drag");
            self.follower.removeClass("active drag");
        });
    }
}