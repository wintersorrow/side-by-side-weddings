import { TimelineMax, Power0 } from "gsap";

export default class Animations {

    loadingScreen(){
        let loadingTimeline = new TimelineMax();

        loadingTimeline
            .set($('.loading__screen--intro .loading__screen--one span'), { autoAlpha: 0, yPercent: '100%'  })
            .set($('.loading__screen--intro .loading__screen--two .background'), { yPercent: '100%', opacity: 1 })
            .set($('.loading__screen--intro .loading__screen--two span'), { autoAlpha: 0  })
            .staggerTo($('.loading__screen--intro .loading__screen--one span'), 1.2, { autoAlpha: 1, yPercent: 0, ease: Power3.easeOut }, 0.2, 2 )
            .to($('.loading__screen--intro .loading__screen--two .background'), 1, { yPercent: 0, ease: Power3.easeOut, onComplete: function() {  $('.loading__screen--intro .loading__screen--one').remove(); } }, 3.6 )
            .to($('.loading__screen--intro .loading__screen--two span'), 1.2, { autoAlpha: 1, ease: Power3.easeOut }, 4 )
            .to($('.loading__screen--intro .loading__screen--two span'), 1.2, { autoAlpha: 0, ease: Power3.easeOut }, 5.2 )
            .to($('.loading__screen--intro .loading__screen'), 1, { autoAlpha: 0, onComplete: function() {  $('.loading__screen--intro .loading__screen-container').remove(); } }, 5.4 )
    }

    loadingScreenIn(){
        let loadingScreenInTimeline = new TimelineMax();

        $('.loading__screen--simple').show();
        $('#header .router-link-active').addClass('leaving');

        loadingScreenInTimeline
            .set($('.loading__screen--simple .loading__screen .background'), { yPercent: '100%' })
            .set($('.loading__screen--simple .loading__screen span'), { autoAlpha: 0 })
            .to($('.loading__screen--simple .loading__screen .background'), 0.6, { yPercent: 0 }, 0 )
            .to($('.loading__screen--simple .loading__screen span'), 0.6, { autoAlpha: 1, ease: Power3.easeOut }, 0.4 )
            .set( '.header__mobile-menu', { height: 0 } )
            .addCallback(function(){ 
                $('html').removeClass('activemenu');
            }, 1 );

        return 1000;
    }

    loadingScreenOut(){
        let loadingScreenOutTimeline = new TimelineMax();

        loadingScreenOutTimeline
            .set($('.loading__screen--simple .loading__screen .background'), { yPercent: 0 })
            .set($('.loading__screen--simple .loading__screen span'), { autoAlpha: 1 })
            .to($('.loading__screen--simple .loading__screen span'), 0.6, { autoAlpha: 0, ease: Power3.easeOut }, 0 )
            .to($('.loading__screen--simple .loading__screen .background'), 0.6, { yPercent: '-100%', onComplete: function() {  $('.loading__screen--simple').hide(); } }, 0.4 );
            
        return 1000;
       
    }

    mobileMenuIn(){
        let mobileMenuTimeline = new TimelineMax();

        $('html').addClass('activemenu');

        mobileMenuTimeline
            .set( '.header__mobile-menu', { height: 0 } )
            .set( '.header__mobile-menu a', { autoAlpha: 0, yPercent: 50 } )
            .to( '.header__mobile-menu', 0.4, { height: $(window).outerHeight() }, 0 )
            .staggerTo( '.header__mobile-menu a', 0.6, { autoAlpha: 1, yPercent: 0, ease: Power3.easeInOut }, 0.2, 0.4 );

        return 2000;
    }

    mobileMenuOut(){
        let mobileMenuTimeline = new TimelineMax();

        mobileMenuTimeline
            .set( '.header__mobile-menu', { height: $(window).outerHeight() })
            .set( '.header__mobile-menu a', { autoAlpha: 1, yPercent: 0 } )
            .staggerTo( '.header__mobile-menu a', 0.6, { autoAlpha: 0, yPercent: 50, ease: Power3.easeInOut }, 0.2, 0 )
            .to( '.header__mobile-menu', 0.4, { height: 0 }, 1.6 )
            .addCallback(function(){ 
                $('html').removeClass('activemenu');
            }, 2 );

        return 2000;
    }

    homepageHide(){
        var homepageTimeline = new TimelineMax();

        homepageTimeline
            .set('.homepage .story .background-image', { autoAlpha: 0 })
            .set('.homepage .featured-stories .social__accounts', { autoAlpha: 0 })
            .set('.homepage .featured-stories .featured-stories__footer', { autoAlpha: 0 })
            .set('.homepage .story .story-heading', { autoAlpha: 0, yPercent: 50 })
            .set('.homepage .story .subtitle h3', { autoAlpha: 1, yPercent: 200 })
            .set('.homepage .story .view-story span.text', { y: '+20px', autoAlpha: 0 })
            .set('.homepage .story .view-story hr', { width: 0 });
    }

    homepageIn( delay, firstPage = false ){
        var homepageTimeline = new TimelineMax();

        if( firstPage ) {
            homepageTimeline
                .set('.homepage .story.swiper-slide-active .story-heading', { autoAlpha: 0, yPercent: 50 })
                .set('.homepage .story.swiper-slide-active .subtitle h3', { autoAlpha: 1 })
                .fromTo('.homepage .story.swiper-slide-active .subtitle h3', 0.4, { yPercent: 200 }, { yPercent: 0, ease: Power3.easeOut }, delay + 0.4 )
                .fromTo('.home .header', 0.6, { autoAlpha: 0 }, { autoAlpha: 1, ease: Power3.easeOut }, delay + 0.4 )
                .fromTo('.homepage .featured-stories__footer', 0.6, { autoAlpha: 0 }, { autoAlpha: 1, ease: Power3.easeOut }, delay + 1 )
                .fromTo('.homepage .social__accounts', 0.6, { autoAlpha: 0 }, { autoAlpha: 1, ease: Power3.easeOut }, delay + 0.4 )
                .staggerTo($('.homepage .story.swiper-slide-active .story-heading'), 1.2, { autoAlpha: 1, yPercent: 0, ease: Power3.easeOut }, 0.2, delay + 0.2 )
                .fromTo('.homepage .story.swiper-slide-active .view-story span.text', 0.6, { y: '+20px', autoAlpha: 0  }, { y: 0, autoAlpha: 1, ease: Power3.easeOut }, delay + 1 )
                .fromTo('.homepage .story.swiper-slide-active .view-story hr', 0.3, { width: 0 }, { width: '25px', ease: Power3.easeOut }, delay + 1.2 )
                .addCallback(function(){ 
                    $('.homepage .story .view-story hr').css({"left": "50%", "transform": "translateX(-50%)"}).addClass('animate');
                    $('.homepage .story .background-image').css({"transition-delay": "1s"});
                }, delay + 1.5 );
        } else {
            $('.homepage .story.swiper-slide-active .background-image').css({"transition-delay": "1s"});

            homepageTimeline
                .set('.homepage .story .background-image', { autoAlpha: 1 })
                .set('.homepage .featured-stories .social__accounts', { autoAlpha: 0 })
                .set('.homepage .featured-stories .featured-stories__footer', { autoAlpha: 0 })
                .set('.homepage .story .story-heading', { autoAlpha: 0, yPercent: 50 })
                .set('.homepage .story .subtitle h3', { autoAlpha: 1, yPercent: 200 })
                .set('.homepage .story .view-story span.text', { y: '+20px', autoAlpha: 0 })
                .set('.homepage .story .view-story hr', { width: 0 })
                .fromTo('.homepage .story.swiper-slide-active .subtitle h3', 0.4, { yPercent: 200 }, { yPercent: 0, ease: Power3.easeOut }, delay + 0.4 )
                .fromTo('.homepage .featured-stories__footer', 0.6, { autoAlpha: 0 }, { autoAlpha: 1, ease: Power3.easeOut }, delay + 1 )
                .fromTo('.homepage .social__accounts', 0.6, { autoAlpha: 0 }, { autoAlpha: 1, ease: Power3.easeOut }, delay + 0.4 )
                .staggerTo($('.homepage .story.swiper-slide-active .story-heading'), 1.2, { autoAlpha: 1, yPercent: 0, ease: Power3.easeOut }, 0.2, delay + 0.2 )
                .fromTo('.homepage .story.swiper-slide-active .view-story span.text', 0.6, { y: '+20px', autoAlpha: 0  }, { y: 0, autoAlpha: 1, ease: Power3.easeOut }, delay + 1 )
                .fromTo('.homepage .story.swiper-slide-active .view-story hr', 0.3, { width: 0 }, { width: '25px', ease: Power3.easeOut }, delay + 1.2 )
                .addCallback(function(){ 
                    $('.homepage .story .view-story hr').css({"left": "50%", "transform": "translateX(-50%)"}).addClass('animate');
                    $('.homepage .story .background-image').css({"transition-delay": "1s"});
                }, delay + 2.5 );

        }

    }

    homepageSlideOut(){
        let homepageSlideOutTimeline = new TimelineMax();

        $('.homepage .story .view-story hr').css({"left": "22.5px", "transform": "translateX(0)", "width" : "0px"}).removeClass('animate')

        homepageSlideOutTimeline
            .set('.homepage .story .background-image', { autoAlpha: 1 })
            .fromTo('.homepage .story .content', 0.4, { autoAlpha: 1 }, { autoAlpha: 0, ease: Power3.easeOut }, 0);
    }

    homepageSlideIn(){
        let homepageSlideInTimeline = new TimelineMax(),
            delay = 0;

        homepageSlideInTimeline
            .set('.homepage .story.swiper-slide-active .view-story hr', { width: 0 })
            .set('.homepage .story.swiper-slide-active .content', { autoAlpha: 1 })
            .set('.homepage .story.swiper-slide-active .story-heading', { autoAlpha: 0, yPercent: '50%' })
            .set('.homepage .story.swiper-slide-active .subtitle h3', { autoAlpha: 1 })
            .set('.homepage .story.swiper-slide-active .view-story hr', { width: 0 })
            .fromTo('.homepage .story.swiper-slide-active .subtitle h3', 0.4, { yPercent: 200 }, { yPercent: 0, ease: Power3.easeOut }, delay + 0.4)
            .staggerTo($('.homepage .story.swiper-slide-active .story-heading'), 1.2, { autoAlpha: 1, yPercent: 0, ease: Power3.easeOut }, 0.2, ( delay + 0.2 ) )
            .fromTo('.homepage .story.swiper-slide-active .view-story span.text', 0.6, { y: '+20px', autoAlpha: 0  }, { y: 0, autoAlpha: 1, ease: Power3.easeOut }, ( delay + 1 ) )
            .fromTo('.homepage .story.swiper-slide-active .view-story hr', 0.3, { width: 0 }, { width: '25px', ease: Power3.easeOut }, ( delay + 1.2 ) );

        homepageSlideInTimeline.addCallback(function(){ 
                $('.homepage .story .view-story hr').css({"left": "50%", "transform": "translateX(-50%)"}).addClass('animate'); 
            }, delay + 1.7 )
    }

    storyIn( delay, fromPage = false ){
        let storyInTimeline = new TimelineMax();

        if( fromPage === false ) {
            setTimeout(function(){
                //Needed so it doesn't fire too soon.
                storyInTimeline
                    .set('.story__header .story-heading', { autoAlpha: 0, yPercent: '50%' })
                    .set('.story__header .subtitle h3', { autoAlpha: 1 })
                    .fromTo('.story__header .background-image', 1.5, { scale: '1.05' }, { scale: '1', ease: "slow(0.7, 0.7, false)" }, delay + 0.5 )
                    .fromTo('.story__header .subtitle h3', 0.4, { yPercent: 200 }, { yPercent: 0, ease: Power3.easeOut }, delay + 1.4 )
                    .staggerTo($('.story__header .story-heading'), 1.2, { autoAlpha: 1, yPercent: 0, ease: Power3.easeOut }, 0.2, delay + 1.2 )
                    .fromTo('.story__header .scroll-story span', 0.6, { y: '+20px', autoAlpha: 0  }, { y: 0, autoAlpha: 1, ease: Power3.easeOut }, delay + 1.8 )
                    .fromTo('.story__content--introduction', 2, { y: '100px', autoAlpha: 0, }, { y: '0', autoAlpha: 1, ease: 'cubic-bezier(.25,.46,.45,.94)' }, delay + 2.4 );
            }, 0 );
        } else {
            if( fromPage == 'Home' ) {
                //Needed so it doesn't fire too soon.
                setTimeout(function(){
                    storyInTimeline
                        .fromTo('.story__header .view-story-container', 0.6, { autoAlpha: 1  }, { autoAlpha: 0, ease: Power3.easeOut }, delay )
                        .fromTo('.story__header .featured-stories__footer', 0.6, { autoAlpha: 1 }, { autoAlpha: 0, ease: Power3.easeOut }, delay )
                        .fromTo('.story__header', 1, { height: '100vh' }, { height: '75vh', ease: Power3.easeOut }, delay + 0.8)
                        .fromTo('.story__header .scroll-story span', 0.6, { y: '+20px', autoAlpha: 0  }, { y: 0, autoAlpha: 1, ease: Power3.easeOut }, delay + 1.4 )
                        .fromTo('.story__content--introduction', 2, { y: '100px', autoAlpha: 0, }, { y: '0', autoAlpha: 1, ease: 'cubic-bezier(.25,.46,.45,.94)' }, delay + 2 );

                    storyInTimeline.addCallback(function(){ 
                        $('.story__header .featured-stories__footer').remove(); 
                    }, delay + 2 )
                }, 0 );

            } else if( fromPage == 'Story' ) {
                //Needed so it doesn't fire too soon.
                setTimeout(function(){
                    storyInTimeline
                        .set('.story__header .story-heading', { autoAlpha: 0, yPercent: '50%' } )
                        .set('.story__header .subtitle h3', { autoAlpha: 1 })
                        .set('.story__content--introduction', { autoAlpha: 0, y: '100px' } )
                        .fromTo('.story__header .background-image', 1.5, { scale: '1.05' }, { scale: '1', ease: "slow(0.7, 0.7, false)" }, delay + 0.5 )
                        .fromTo('.story__header .subtitle h3', 0.4, { yPercent: 200 }, { yPercent: 0, ease: Power3.easeOut }, delay + 1.4 )
                        .staggerTo($('.story__header .story-heading'), 1.2, { autoAlpha: 1, yPercent: 0, ease: Power3.easeOut }, 0.2, delay + 1.2 )
                        .fromTo('.story__header .scroll-story span', 0.6, { y: '+20px', autoAlpha: 0  }, { y: 0, autoAlpha: 1, ease: Power3.easeOut }, delay + 1.8 )
                        .fromTo('.story__content--introduction', 2, { y: '100px', autoAlpha: 0, }, { y: '0', autoAlpha: 1, ease: 'cubic-bezier(.25,.46,.45,.94)' }, delay + 2.4 );
                }, 0 );
            }
        }
    }

    portfolioIn(introTime){
        let portfolioTimeline = new TimelineMax();

        portfolioTimeline
            .set('.gallery .image', { autoAlpha: 0, yPercent: '50%' })
            .staggerTo($('.gallery .image'), 1.2, { autoAlpha: 1, yPercent: 0, ease: Power3.easeOut }, 0.2, ( introTime + 0.2 ) );
    }

    aboutIn(){
        let aboutInTimeline = new TimelineMax();

        aboutInTimeline
            .fromTo('.about__wrapper', 2, { y: '100px', autoAlpha: 0, }, { y: 0, autoAlpha: 1, ease: Power3.easeOutCubic }, 0.5);

        return 1500;
    }

    contactIn(){
        let contactInTimeline = new TimelineMax();

        // contactInTimeline
        //     .fromTo('.contact__wrapper', 2, { y: '100px', autoAlpha: 0, }, { y: 0, autoAlpha: 1, ease: Power3.easeOutCubic }, 0.5);

        return 500;
    }

    fourohfourIn(){
        let fourohfourInimeline = new TimelineMax();

        fourohfourInimeline
            .fromTo('.fourohfour__wrapper', 2, { y: '100px', autoAlpha: 0, }, { y: 0, autoAlpha: 1, ease: Power3.easeOutCubic }, 0.5);

        return 1500;
    }

}