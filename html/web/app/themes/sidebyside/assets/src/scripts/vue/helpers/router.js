import { createRouter, createWebHistory } from 'vue-router'

import SBS404 from "../views/404.vue";
import SBSHome from "../views/home.vue";
import SBSStory from "../views/story.vue";
import SBSPortfolio from "../views/portfolio.vue";
import SBSAbout from "../views/about.vue";
import SBSContact from "../views/contact.vue";

const router = createRouter({
    routes: [
        {
            path: "/",
            name: "Home",
            component: SBSHome
        },
        {
            path: "/stories/:postSlug",
            name: "Story",
            component: SBSStory
        },
        {
            path: "/portfolio",
            name: "Portfolio",
            component: SBSPortfolio
        },
        {
            path: "/our-approach",
            name: "About",
            component: SBSAbout
        },
        {
            path: "/contact",
            name: "Contact",
            component: SBSContact
        },
        {
            path: "/:pageSlug",
            name: "404",
            component: SBS404
        },
    ],
    history: createWebHistory()
});

// This callback runs before every route change, including on page load.
router.beforeEach((to, from, next) => {
    setTimeout(function(){
        next();
    }, 1000);
});

export default router;