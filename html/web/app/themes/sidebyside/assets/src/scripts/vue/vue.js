import $ from 'jquery';

import router from './helpers/router';
import store from './helpers/store';
import { mapState, mapMutations } from 'vuex'; 

import Animations from '../standalone/animations';
window.Animations = new Animations();

import IntroScreen from './views/intro-screen';
import LoadingScreen from './views/loading-screen';
import GlobalHeader from './blocks/header';

window.templatePath = '/app/themes/sidebyside/assets/dist/';

if( $('#sbsroot').length > 0 ) {
    const app = createApp({
        components: {
            IntroScreen,
            LoadingScreen
        },
        created() {
            $('body').addClass('loading');
            AOS.init({
                once: true,
            });
        },
        mounted() {
            let vm = this;

            if( vm.$store !== undefined && vm.$store.state.show_intro ) {
                window.Animations.loadingScreen();

                setTimeout(function(){
                    vm.$store.dispatch( 'updateIntro', false );
                    $('body').removeClass('loading');
                }, 6000);
            }

            axios.get('/wp-json/acf/v3/options/general-settings')
                .then( response => { vm.$store.dispatch('updateSettings', response.data.acf ); } )
                .catch( error => ( console.log(error) ) );

        }
    });

    app.use(router);
    app.use(store);
    app.mount('#sbsroot');
}

if( $('#header').length > 0 ) {
    const header = createApp({
        components: {
            GlobalHeader
        },
    });

    header.use(router);
    header.use(store);
    header.mount('#header');
}