let mix = require('laravel-mix');
require('laravel-mix-clean');
require('laravel-mix-imagemin');

// Set dist path
mix.setPublicPath('web/app/themes/sidebyside/assets/dist/');

// Clean dist folder
mix.clean();

// Compile SCSS and disable CSS URL rewriting
mix.sass('web/app/themes/sidebyside/assets/src/styles/style.scss', 'styles', {
    processUrls: false,
    autoprefixer: {
        options: {
            browsers: [
                'last 4 versions',
            ],
            grid: "no-autoplace",
            flexbox: true
        }
    }
});

mix.js('web/app/themes/sidebyside/assets/src/scripts/scripts.js', 'scripts').extract().vue();

mix.copy('web/app/themes/sidebyside/assets/src/images', 'web/app/themes/sidebyside/assets/dist/images');
mix.copy('web/app/themes/sidebyside/assets/src/scripts/lazysizes.min.js', 'web/app/themes/sidebyside/assets/dist/scripts/lazysizes.min.js');

mix.copyDirectory('web/app/themes/sidebyside/assets/src/fonts', 'web/app/themes/sidebyside/assets/dist/fonts');

// Add sourcemaps
mix.sourceMaps(false, 'source-map');

// Disable OS notifications for successful builds
mix.disableSuccessNotifications();

mix.webpackConfig({
    resolve: {
        modules: ['node_modules'],
        alias: {
            "TweenLite": "gsap/src/uncompressed/TweenLite.js",
            "TweenMax": "gsap/src/uncompressed/TweenMax.js",
            "TimelineLite": "gsap/src/uncompressed/TimelineLite.js",
            "TimelineMax": "gsap/src/uncompressed/TimelineMax.js",
            "ScrollMagic": 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
            "animation.gsap": 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js',
            'CSSRulePlugin': "gsap/CSSRulePlugin.js",
            'MorphSVGPlugin': 'gsap/MorphSVGPlugin.js',
        }
    }
});

mix.browserSync('sidebysideweddings.co.uk.test');